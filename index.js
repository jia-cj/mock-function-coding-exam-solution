function countLetter(letter, sentence) {

    if (typeof letter !== 'string' || letter.length !== 1 || !letter.match(/[a-zA-Z]/)) {
        return undefined; // Invalid letter
    }

    const letterToSearch = letter.toLowerCase();
    const sentenceLowerCase = sentence.toLowerCase();
    let count = 0;

    for (let i = 0; i < sentenceLowerCase.length; i++) {
        if (sentenceLowerCase[i] === letterToSearch) {
            count++;
        }
    }

    return count;
}

function isIsogram(text) {
    
    const textLowerCase = text.toLowerCase();
    const charSet = new Set();

    for (let char of textLowerCase) {
        if (charSet.has(char)) {
            return false;
        }
        charSet.add(char);
    }

    return true;
}

function purchase(age, price) {
    if (age < 13) {
        return undefined;

    } else if (age >= 13 && age <= 21) {
        const discountedPrice = price * 0.8;
        return discountedPrice.toFixed(2);

    } else if (age >= 22 && age <= 64) {
        
        return price.toFixed(2);

    } else if (age >= 65) {
        const discountedPrice = price * 0.9;
        return discountedPrice.toFixed(2);

    } else {
        
        return undefined;
    }
}



function findHotCategories(items) {
    const hotCategories = new Set();

    
    for (const item of items) {
        if (item.stocks === 0) {
            hotCategories.add(item.category);
        }
    }

    return Array.from(hotCategories);
}

function findFlyingVoters(candidateA, candidateB) {
    const commonVoters = [];

   
    for (const voterA of candidateA) {
        
        if (candidateB.includes(voterA)) {
            commonVoters.push(voterA);
        }
    }

    return commonVoters;
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};